;; This file is part of racket-rawk - AWK-like scripting in Racket.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-rawk is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-rawk is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-rawk.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           rawk/parse)

  (test-equal? "Match nothing"
               (conditions-parse "" "" { (display "z") })
               "")

  (test-equal? "Match all with regex"
               (conditions-parse "\n" "" .* { (display "z") })
               "z")

  (test-equal? "Match all with special condition"
               (conditions-parse "\n" "" { (display "z") })
               "z")

  (test-equal? "Match all and special condition give the same result #1"
               (conditions-parse "\n" "" .* { (display "z") })
               (conditions-parse "\n" "" { (display "z") }))

  (test-equal? "Match all and special condition give the same result #2"
               (conditions-parse "\n"
                                 ""
                                 .* { (display "z") } { (display "z") })
               "zz")

  (test-equal? "Display 1 using END"
               (conditions-parse "" "" END { (display 1) })
               "1")

  (test-equal? "Display 12 using BEGIN and END"
               (conditions-parse ""
                                 ""
                                 BEGIN { (display 1) } END { (display 2) })
               "12")

  (test-equal? "Display z's value using BEGIN and END"
               (conditions-parse ""
                                 ""
                                 BEGIN { (define z 1) } END { (display z) })
               "1")

  (test-equal? "Count liens with \"z\"s - 0"
               (conditions-parse ""
                                 ""
                                 BEGIN { (define zs 0) }
                                 END { (display zs) }
                                 z { (set! zs (+ zs 1))})
               "0")

  (test-equal? "Count liens with \"z\"s - 1"
               (conditions-parse "z\n"
                                 ""
                                 BEGIN { (define zs 0) }
                                 END { (display zs) }
                                 z { (set! zs (+ zs 1))})
               "1")

  (test-equal? "Count liens with \"z\"s - 3"
               (conditions-parse "z\n\nx\nc\nz\nz\n"
                                 ""
                                 BEGIN { (define zs 0) }
                                 END { (display zs) }
                                 z { (set! zs (+ zs 1))})
               "3"))
