;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket))


@title[#:tag "rawk-syntax"]{Syntax}


The following describers RAWK language syntax, how to write
conditions and differences between RAWK and AWK.


@section[#:tag "rawk-syntax-conditions"]{Conditions}

@racketgrammar*[
 #:literals (IMPORT BEGIN END FROM)
 [condition
  import-expression
  begin-expression
  end-expression
  matchall-expression
  match-expression
  ...]
 [import-expression
  (~seq IMPORT import-body)]
 [begin-expression
  (~seq BEGIN expression-body)]
 [end-expression
  (~seq END expression-body)]
 [matchall-expression
  expression-body]
 [match-expression
  (~seq symbol-regexp expression-body)]
 [import-body
  (import-mod ...)]
 [expression-body
  (body ...)]
 ]

@itemlist[
 @item{@racket[~seq] means that the inside identifiers appear as a sequence
  and they are not surrounded by parentheses,}
 @item{@racketid[import-name] and @racketid[import-statement]
  are identifiers referring to a identifier imported from a module
  and the module the identifier should be imported from,}
 @item{@racketid[symbol-regexp] is a symbol treated as a @racket[regexp],
  for example @racketid[.*] will become @racket[#rx".*"],}
 @item{@racketid[body] is any racket expression.}
 ]


@section[#:tag "rawk-syntax-differences"]{RAWK vs AWK}

@itemlist[
 @item{condition actions (@racketid[body]s) are pure Racket syntax}
 @item{conditions are surrounded by "||" rather than "//",

  In RAWK:

  @nested[#:style 'code-inset]{@verbatim{
  |a.*l| {
      (print "!")
  }
  }}

  In AWK:

  @nested[#:style 'code-inset]{@verbatim{
  /a.*l/ {
      print "!";
  }
  }}

  "||" is also optional,
  it encapsulates the @racketid[symbol-regexp]
  similarly to how it can encapsulate symbols in Racket,
  so if there are no characters that are specially treated
  in the @racketid[symbol-regexp], then "||" can be omitted,
  }
 @item{there is no @racketid[function] statement,
  custom functions used inside condition @racketid[body]s should be instead
  defined inside @racket[BEGIN] pseudo-condition,

  @nested[#:style 'code-inset]{@verbatim{
  BEGIN {
      (define (smile)
        (displayln ":-)"))
  }
  {
      (smile)
  }
  }}}
 @item{@racketid[IMPORT] can be used to import Racket code defined elsewhere.}
 ]
