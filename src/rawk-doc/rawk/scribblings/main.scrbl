;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require ziptie/git/hash
          rawk/version)


@title[#:tag "rawk"]{RAWK}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


AWK-like scripting in Racket.

@(define upstream-tree
   "https://gitlab.com/xgqt/racket-rawk/-/tree/")

Version: @link[@(string-append upstream-tree @VERSION)]{@VERSION},
commit hash:
@(let ([git-hash (git-get-hash #:short? #t)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (string-append upstream-tree git-hash) git-hash)]))


@table-of-contents[]

@include-section{stx.scrbl}
@include-section{cli.scrbl}
@include-section{parse.scrbl}
@include-section{eval.scrbl}
@include-section{lang.scrbl}

@index-section[]
