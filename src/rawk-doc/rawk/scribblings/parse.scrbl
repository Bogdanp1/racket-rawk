;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket rawk/parse))


@title[#:tag "rawk-parse"]{Parse}

@defmodule[rawk/parse]

@defform[
 (make-conditions-parser conditions-syntax ...)
 ]{
 Returns a @racket[lambda] function that takes 2 arguments:

 @itemlist[
  @item{
   @racketid[input-port] (@racket[input-port?]),
  }
  @item{
   @racketid[separator] (@racket[string?], later passed to @racket[regexp]).
  }
 ]

 See @seclink["rawk-syntax-conditions"]{RAWK conditions} documentation
 for @racket[conditions-syntax] reference.
}

@defform[
 (conditions-parse input-string separator conditions-syntax ...)
 ]{
 Returns a @racket[string] that is the result of output from
 @racket[make-conditions-parser] call used internally in this macro.

 See @seclink["rawk-syntax-conditions"]{RAWK conditions} documentation
 for @racket[conditions-syntax] reference.
}
