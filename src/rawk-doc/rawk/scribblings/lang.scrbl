;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label (except-in racket print)
                     rawk))


@title[#:tag "rawk-lang"]{The RAWK Language}


@defmodulelang[rawk]{

The @racketmodname[rawk] language.

RAWK can be used as a so-called "hash-lang"
because of this RAWK files can be @racket[require]d by other Racket code.

When @racket[require]d, a "#lang rawk" file provides only
the @racketid[transform] @racket[procedure].

The @racketid[transform] function is created by
@racket[make-conditions-parser].

The acceptable synatx is defined @seclink["rawk-syntax"]{here}.
}
