;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket rawk/eval rawk/parse))


@title[#:tag "rawk-eval"]{Eval}

@defmodule[rawk/eval]

@defproc[
 (string->conditions-parser
  [rawk-string string?])
 (-> input-port? string? void?)
 ]{
 Creates a conditions parser from a given @racket[rawk-string] @racket[string]
 like @racket[make-conditions-parser] does from @racket[syntax].
}

@defproc[
 (string-conditions-parse
  [rawk-string string?]
  [input-string string?]
  [separator string?])
 string?
 ]{
}
