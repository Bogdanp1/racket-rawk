;; This file is part of racket-req - project dependency manager.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-req is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-req is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-req.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (only-in scribble/bnf nonterm))


@title[#:tag "rawk-cli"]{Command-line interface}


@section[#:tag "req-cli-synopsis"]{Synopsis}

RAWK invocation in any one of the following forms is accepted:

@itemlist[
 @item{@exec{rawk [flag] ...} --- reads standard input,}
 @item{@exec{rawk [flag] ... <file> ...} --- reads given files,}
 @item{@exec{<command> | rawk [flag] ...} --- reads from pipe.}
 ]


@section[#:tag "req-cli-flags"]{Flags}

@itemlist[
 @item{
  @Flag{F} @nonterm{fs} or @DFlag{field-separator} @nonterm{fs}
  --- use @nonterm{fs} for the input field separator,
 }
 @item{
  @Flag{D} or @DFlag{debug}
  --- turn on the debugging options,
 }
 @item{
  @Flag{h} or @DFlag{help}
  --- show the help page,
 }
 @item{
  @Flag{V} or @DFlag{version}
  --- show the version of this program and exit.
 }
 ]
