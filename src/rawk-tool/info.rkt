#lang info


(define pkg-desc "AWK-like scripting in Racket. Tool.")

(define version "1.2")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "rawk-lib"))

(define build-deps
  '())
