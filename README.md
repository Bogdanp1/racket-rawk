# Rawk

<p align="center">
    <a href="http://pkgs.racket-lang.org/package/rawk">
        <img src="./extras/badges/raco_pkg_install-rawk-aa00ff.svg">
    </a>
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-rawk">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-rawk/">
    </a>
    <a href="https://gitlab.com/xgqt/racket-rawk/pipelines">
        <img src="https://gitlab.com/xgqt/racket-rawk/badges/master/pipeline.svg">
    </a>
</p>

AWK-like scripting in Racket.

<p align="center">
    <img src="logo.png" width="250" height="250">
</p>


## About

RAWK is modeled after the famous UNIX tool AWK,
the pattern scanning and processing language.


## Installation

### Raco

Use raco to install RAWK from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user rawk
```

### Req

Use Req to install RAWK from its project directory.

``` sh
raco req --everything --verbose
```

### Make

Use GNU Make to install RAWK from its project directory.

```sh
make install
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either [GitLab pages](https://xgqt.gitlab.io/racket-rawk/)
or [Racket-Lang Docs](https://docs.racket-lang.org/rawk/).


## License

Code in this project is licensed under the GPL, version 2.0 (or later) license.

The RAWK logo contributed by
[Madiana Argon](https://is-a.cat/@madargon)
is licensed under the CC BY-SA 4.0 license.

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
